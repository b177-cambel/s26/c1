// create an aggregate that will get the average of stocks of fruits on sale
db.fruits.aggregate([
    { $match: { onSale: true } },
    { $group: { _id: "fruitsonSale", aveStocks: { $avg: "$stock" } } }
]);

// get the average stocks of fruits based on specific suppliers
db.fruits.aggregate([
    { $group: { _id: "$supplier_id", aveStocks: { $avg: "$stock" } } }
]);


// create an aggregate that will get the maximum price of fruits on sale
db.fruits.aggregate([
    { $match: { onSale: true } },
    { $group: { _id: "fruitsonSale", maxPrice: { $max: "$price" } } }
]);

// get the max price in origin "philippines"
db.fruits.aggregate([
    { $match: { origin: "Philippines" } },
    { $group: { _id: "originPhilippines", maxPrice: { $max: "$price" } } }
]);
